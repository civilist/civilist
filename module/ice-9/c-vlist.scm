(define-module (ice-9 c-vlist)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export(vlist? vlist-cons vlist-head vlist-tail vlist-null?
                  vlist-null list->vlist vlist-ref vlist-set!
		  vlist-fold vlist-fold-right 
                  vlist-last-val vlist-cons* vlist-pair?
                  test-vlist
		  vlist-drop vlist-take
		  vlist-length  vlist-map
		  ;vlist-unfold vlist-unfold-right vlist-append
		  vlist-reverse vlist-filter vlist-delete vlist->list
		  vlist-for-each

                  vlist-truncate! vhash-truncate!
                  vlist-thread-inc vlist-new-thread
                  vlist-refcount-- vlist-refcount++

                  vhash? vhash-cons vhash-consq vhash-consv vhash-consa
                  vhash-assoc vhash-assq vhash-assa vhash-assv
                  vhash-delete vhash-delq vhash-delv
                  vhash-fold vhash-fold-right
                  vhash-set! vhash-setq! vhash-setv!
                 ;vhash-fold* vhash-foldq* vhash-foldv*
                 ;alist->vhash

                  vhash->assoc
		  block-growth-factor init-block-size
		  vhash-ref vhashq-ref vhasha-ref))

(let ((file  (%search-load-path "ice-9/vlist/libguile-vlist.so")))
  (if file
      (load-extension file "vlist_init")
      (error "libguile-vlist.so is not present, did you forget to make it?")))

(define (vhash-ref h x e)
  (if (vhash? h)
      (let ((r (vhash-assoc x h)))
	(if r
	    (cdr r)
	    e))
      e))

(define (vhashq-ref h x e)
  (if (vhash? h)
      (let ((r (vhash-assq x h)))
	(if r
	    (cdr r)
	    e))
      e))

(define (vhasha-ref h x e)
  (if (vhash? h)
      (let ((r (vhash-assa x h)))
	(if r
	    (cdr r)
	    e))
      e))

(define-record-type <vlist>
  ;; A vlist is just a base+offset pair pointing to a block.

  ;; XXX: Allocating a <vlist> record in addition to the block at each
  ;; `vlist-cons' call is inefficient.  However, Bagwell's hack to avoid it
  ;; (Section 2.2) would require GC_ALL_INTERIOR_POINTERS, which would be a
  ;; performance hit for everyone.
  (make-vlist base offset)
  vlist?
  (base    vlist-base)
  (offset  vlist-offset))

(define x                   (setup-vlist <vlist>))
(define vlist-null          (list-ref x 0))
(define block-growth-factor (list-ref x 1))
(define init-block-size     (list-ref x 2))
(define thread-seq          (list-ref x 3))
(define thread-nr           (list-ref x 4))
(define thread-inc          (list-ref x 5))
(define (vlist-pair? x) (and (vlist? x) (not (vlist-null? x))))

(define (vlist-thread-inc)
  (fluid-set! thread-seq (+ thread-inc (fluid-ref thread-seq))))

(define (vlist-new-thread)
  (fluid-set! thread-nr  (+ thread-inc (fluid-ref thread-nr)))
  (fluid-set! thread-seq thread-inc))

(define (vlist-last-val x)
  (if (vlist-pair? x)
      (vlist-ref x (- (vlist-length x) 1))
      #f))

(define (vlist-cons* . l)
  (let ((r (vlist-last-val l)))
    (if r
        (let lp ((l l))
          (if (eq? l r)
              r
              (vlist-cons (car l) (lp (cdr l)))))
        r)))

(set-record-type-printer! <vlist>
                          (lambda (vl port)
                            (cond ((vlist-null? vl)
                                   (format port "#<vlist ()>"))
                                  ((vhash? vl)
                                   (format port "#<vhash ~x ~a pairs>"
                                           (object-address vl)
                                           (vlist-length vl)))
                                  (else
                                   (format port "#<vlist ~a>"
                                           (vlist->list vl))))))
